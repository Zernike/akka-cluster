scalaVersion := "2.11.6"
name := "Test Cluster Task"
version := "0.1.0"

val akkaVersion = "2.3.9"

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion withSources,
//    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-remote" % akkaVersion withSources
  )
