package system

import system.ClusterNode.AliveNode

sealed trait ClusterTask
case class NewMember(node: AliveNode) extends ClusterTask
