package system

import akka.actor._
import akka.event.Logging
import system.ClusterNode._
import scala.concurrent.duration._

class ClusterNode(nodes: IdleData, selfAddress: Address) extends LoggingFSM[ClusterState, Data] {
  val logger = Logging(context.system, this)

  private def sendIdentifyRequest(address: Address): Unit = {
    context.actorSelection(addr2path(address)) ! Identify(address)
  }
  private def addr2path(addr: Address): String = s"$addr/user/$name"

  /**
   * Could not find any alive node or get invitation to cluster.
   */
  when(Idle, 50.millis) {
    case Event(ClusterParticipationInvite(data), _) =>
      goto(ClusterMode) using data
    case Event(StateTimeout, d: IdleData) if !d.unresolved.exists(_ != selfAddress) =>
      logger.debug("There is no addressee. Create cluster.")
      goto(ClusterMode) using ClusterData(Set(AliveNode(selfAddress, self)), Map())
    case Event(req: ClusterParticipationRequest, _) =>
      stay() replying SingletonNode
    case Event(StateTimeout, d: IdleData)  =>
      d.unresolved filterNot (_ == selfAddress) foreach sendIdentifyRequest
      logger.debug("Go to Connecting from Idle")
      goto(Connecting)
  }

  /**
   * Cluster member
   */
  when(ClusterMode) {
    case Event(ClusterParticipationInvite(data), d: ClusterData) =>
      //ToDo logger.error("resolve fragmentation")
      stay()
    case Event(ClusterTick, d: ClusterData) =>
      context.actorOf(ClusterValidationActor.props(d.allNodes))
      d.commands map {
        case (NewMember(node), consensus) =>
          node.ref ! Identify(node)
          (NewMember(node), consensus)
      }
      stay() using d
    case Event(UnavailableNode(lostNode), d: ClusterData) =>
      val nodes = d.allNodes - lostNode
      val commands = d.commands map {
          case (k,v) => (k, v excludeNode lostNode)
        }
      stay() using ClusterData(nodes, commands)
    case Event(req: ClusterParticipationRequest, d: ClusterData) =>
      logger.debug(s"$selfAddress get $req")
      context.actorOf(RecruitValidationActor.props(req.asker))
      stay().
        using(d.copy(commands = d.commands + (NewMember(req.asker) -> zeroConsensus(d.allNodes)))).
        replying(WaitDecision)
    case Event(AllowRecruit(node: AliveNode), d: ClusterData) =>
      val consensus = d.commands(NewMember(node))
      val data: ClusterData = if (consensus isLast self) {
        consensus updateConsensus(self, Some(true))
        d.allNodes.foreach(_.ref ! d.copy(commands = d.commands + (NewMember(node) -> consensus)))
        consensus.checkConsensus map {
          if (_) {
            val newData = ClusterData(d.allNodes + node, d.commands filterKeys (_ != NewMember(node)))
            node.ref ! ClusterParticipationInvite(newData)
            newData.allNodes foreach (_.ref ! newData)
            newData
          }
          else {
            node.ref ! ClusterParticipationDenied
            val newData = ClusterData(d.allNodes, d.commands filterKeys (_ != NewMember(node)))
            newData.allNodes foreach (_.ref ! newData)
            newData
          }
        }
      }.get else {
        val data = d.copy(commands = d.commands + (NewMember(node) -> consensus.updateConsensus(self, Some(true))))
        d.allNodes.foreach(_.ref ! data)
        data
      }
      stay() using data
    case Event(DenyRecruit(node: AliveNode), d: ClusterData) =>
      val c = d.commands.get(NewMember(node)) map (x => x.filter())
      val commands = if (c.isEmpty) d.commands else d.commands + (NewMember(node) ->)
      stay() using d.copy(commands = d.commands +)
  }

  /**
   * Collecting alive nodes statuses and waiting cluster invitation (or create cluster if needed).
   */
  when(ClusterJoining, 50.millis) {
    case Event(StateTimeout, IdleData(unres, fail, alive)) if alive.exists(_._2 contains ClusterMember) =>
      goto(Idle) using IdleData(fail ++ alive.map(_._1.address))
    case Event(StateTimeout, IdleData(unres, fail, alive)) =>
      val data = ClusterData(Set(AliveNode(selfAddress, self)), Map())
      alive.filter(_._2.nonEmpty).map(_._1.ref).foreach (_ ! ClusterParticipationInvite(data))
      goto(ClusterMode) using data
    case Event(ClusterParticipationInvite(data), _) =>
      goto(ClusterMode) using data
    case Event(SingletonNode, IdleData(unres, fail, alive)) =>
      val node = alive.find(_._1.ref == sender).get
      stay() using IdleData(unres, fail, alive + (node._1 -> Some(Singleton)))
    case Event(WaitDecision, IdleData(unres, fail, alive)) =>
      val node = alive.find(_._1.ref == sender).get
      stay() using IdleData(unres, fail, alive + (node._1 -> Some(ClusterMember)))
  }

  /**
   * Ask all addresses from unresolved set.
   * If any alive would be found then go to ClusterJoining (ask cluster invite), another - create cluster.
   */
  when(Connecting, 50.millis) {
    case Event(ClusterParticipationInvite(data), _) =>
      goto(ClusterMode) using data
    case Event(StateTimeout, IdleData(unres, fail, alive)) if alive.isEmpty =>
      goto(ClusterMode) using ClusterData(Set(AliveNode(selfAddress, self)), Map())
    case Event(StateTimeout, IdleData(unres, fail, alive)) if alive.nonEmpty =>
      alive foreach (_._1.ref ! ClusterParticipationRequest(AliveNode(selfAddress, self)))
      goto(ClusterJoining) using IdleData(Set(), unres ++ fail, alive)
    case Event(ActorIdentity(addr: Address, Some(actorRef)), IdleData(unres, fail, alive)) =>
      logger.info(s"Connected to $addr")
      stay() using IdleData(unres - addr, fail, alive + (AliveNode(addr, actorRef) -> None) )
    case Event(ActorIdentity(addr: Address, None), IdleData(unres, fail, alive)) =>
      logger.warning(s"Could not connect to $addr")
      stay() using IdleData(unres - addr, fail + addr, alive)
  }

  onTransition {
    case Idle -> Connecting                   => logger.debug("From Idle to Connecting state.")
    case _ -> ClusterMode              =>
      setTimer("cluster timer", ClusterTick, 100.millis, repeat = true)
      logger.debug(s"$selfAddress in ClusterModePassive state.")
    case Connecting -> ClusterJoining         => logger.debug("From Connecting to ClusterJoining state.")
    case ClusterJoining -> Idle               => logger.debug("From ClusterJoining to Idle state.")
  }

  startWith(Idle, nodes)
  initialize()
}

object ClusterNode {
  val name = "ClusterNode"
  def props(nodes: Set[Address], self: Address) = Props(new ClusterNode(IdleData(nodes), self))

  sealed trait ClusterState
  case object Idle extends ClusterState
  case object Connecting extends ClusterState
  case object ClusterJoining extends ClusterState
  case object ClusterMode extends ClusterState
  case object ClusterError extends ClusterState

  sealed trait Data

  /**
   * Single mode data
   */
  case class IdleData(unresolved: Set[Address],
                     fail: Set[Address]  = Set(),
                     alive: Map[AliveNode, Option[NodeStatus]] = Map()) extends Data {
    def empty = IdleData(Set())
  }

  /**
   * Cluster mode data
   */
  case class ClusterData(allNodes: Set[AliveNode],
                         commands: Map[ClusterTask, ClusterConsensus]) extends Data

  case class ClusterConsensus(cons: Map[AliveNode, Option[Boolean]]) {
    def checkConsensus: Option[Boolean] =
      cons.foldLeft(Some(true): Option[Boolean]) { case (accum, el) =>
        for {
          a <- accum
          b <- el._2
        } yield a && b
      }
    def isLast(ref: ActorRef): Boolean = {
      val node: AliveNode = (cons find (_._1.ref == ref) map (_._1)).get
      cons(node).isEmpty && (cons filterKeys (_ != node) forall (_._2.nonEmpty))
    }
    def updateConsensus(ref: ActorRef, decision: Option[Boolean]): ClusterConsensus = {
      val node: AliveNode = (cons find (_._1.ref == ref) map (_._1)).get
      ClusterConsensus(cons + (node -> decision))
    }
    def excludeNode(node: AliveNode): ClusterConsensus = ClusterConsensus(cons filterKeys (_ != node))
    def includeNode(node: AliveNode): ClusterConsensus = ClusterConsensus(cons + (node -> None))
  }
  private def zeroConsensus(nodes: Set[AliveNode]): ClusterConsensus =
    ClusterConsensus(nodes.zip(List.fill(nodes.size)(None)).toMap)

  case class AliveNode(address: Address, ref: ActorRef)

  sealed trait NodeStatus
  object ClusterMember extends NodeStatus
  object Singleton extends NodeStatus

}
