package system

import akka.actor._
import system.ClusterNode.AliveNode
import scala.concurrent.duration._

class RecruitValidationActor(node: AliveNode) extends Actor {
  node.ref ! Identify(node.address)
  context setReceiveTimeout 50.millis

  def receive = {
    case ReceiveTimeout =>
      context.parent ! DenyRecruit(node)
      context.stop(self)
    case ActorIdentity(addr: Address, Some(actorRef)) =>
      context.parent ! AllowRecruit(node)
      context.stop(self)
  }
}

object RecruitValidationActor {
  def props(node: AliveNode) = Props(new RecruitValidationActor(node))
}
