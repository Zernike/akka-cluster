package system

import system.ClusterNode.{AliveNode, ClusterData}

sealed trait ClusterCommand
case class ClusterParticipationRequest(asker: AliveNode) extends ClusterCommand
case class ClusterParticipationInvite(data: ClusterData) extends ClusterCommand
case object ClusterParticipationDenied extends ClusterCommand
case object SingletonNode extends ClusterCommand
case object WaitDecision extends ClusterCommand
case object ClusterTick extends ClusterCommand
case class UnavailableNode(node: AliveNode) extends ClusterCommand
case class AllowRecruit(node: AliveNode) extends ClusterCommand
case class DenyRecruit(node: AliveNode) extends ClusterCommand