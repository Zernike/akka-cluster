package system

import akka.actor._
import scala.concurrent.duration._
import system.ClusterNode.AliveNode

class ClusterValidationActor(nodes: Set[AliveNode]) extends Actor {
  var clusterMembers: Set[AliveNode] = nodes
  nodes foreach (x => x.ref ! Identify(x.address))
  context.system.scheduler.scheduleOnce(50.millis, self, PoisonPill)

  def receive = {
    case ActorIdentity(addr: Address, Some(actorRef)) =>
      clusterMembers = clusterMembers filterNot (_.address == addr)
  }

  override def postStop(): Unit = {
    clusterMembers foreach (context.parent ! UnavailableNode(_))
  }
}

object ClusterValidationActor {
  def props(nodes: Set[AliveNode]) = Props(new ClusterValidationActor(nodes))
}
