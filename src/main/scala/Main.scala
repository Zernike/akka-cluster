import akka.actor.{ActorSystem, Address, AddressFromURIString}
import akka.event.Logging
import com.typesafe.config.{ConfigException, Config, ConfigFactory}
import system.ClusterNode

import scala.util.{Failure, Try}

object Main {

  def main(args: Array[String]): Unit = {
    val systemName: String = systemNameFromArgs(args)
    val config = ConfigFactory.load(configName(args))
    val system = ActorSystem(systemName, config)
    val logger = Logging(system, "main")

    val selfAddress = getOwnAddressFrom(config, systemName) recover {
      case err: ConfigException =>
        logger.error(s"Necessary configuration is absent.\n\t$err")
        sys.exit(1)
    }
    system.actorOf(ClusterNode.props(seedsFromArgs(args), selfAddress.get), ClusterNode.name)
    logger.info(s"ActorSystem $systemName started.")
  }

  private def getOwnAddressFrom(config: Config, systemName: String): Try[Address] = {
    for {
      host <- Try(config.getString("akka.remote.netty.tcp.hostname"))
      port <- Try(config.getString("akka.remote.netty.tcp.port"))
    } yield AddressFromURIString(s"akka.tcp://$systemName@$host:$port")
  }

  private def configName(args: Array[String]): String = {
    val argGroup = "-ConfigName:"
    args.
      find(_.startsWith(argGroup)).
      map(_.split(argGroup)(1)).
      getOrElse("application")
  }

  private def systemNameFromArgs(args: Array[String]): String = {
    val argGroup = "-ActorSystem:"
    args.
      find(_.startsWith(argGroup)).
      map(_.split(argGroup)(1)).
      getOrElse("ClusterBeer")
  }

  private def seedsFromArgs(args: Array[String]): Set[Address] = {
    val argGroup = "-Nodes:"
    val delimiter = ","
    val nodeArgs: Set[String] = args.toSet.
      filter(_.startsWith(argGroup)).
      map(_.split(argGroup)(1).split(delimiter).toSet).
      flatMap(x=>x)
    nodeArgs map {
      case arg if arg.matches( """^\w+@[0-9A-Za-z.-]+:\d{1,5}$""") =>
        Try(AddressFromURIString(s"akka.tcp://$arg"))
      case _ => Failure(new IllegalArgumentException)
    } filter (_.isSuccess) map (_.get)
  }

}
